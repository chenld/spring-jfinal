#spring-jfinal
作用于 spring 和 jfinal 的整合。
## 1、web.xml
```xml
    <filter>
        <filter-name>jfinal</filter-name>
    	<filter-class>com.jfinal.core.SpringJFinalFilter</filter-class>
    	<init-param>
    		<param-name>configClass</param-name>
    		<param-value>demo.run.JfinalConfig</param-value>
    	</init-param>
    	<!-- 参数有 contextConfigLocation
    		、configurations
    		、annotatedClasses
    		、basePackages
    		四选一，支持  context-param,优先 init-param.
    		其中 configurations 相当于 PathKit.getWebRootPath() + "/" + WEB-INF/spring.xml，支持多个配置文件以 "," 分割。
    	 -->
    	<init-param>
    		<param-name>configurations</param-name>
    		<param-value>WEB-INF/spring.xml</param-value>
    	</init-param>
    </filter>
    <filter-mapping>
    	<filter-name>jfinal</filter-name>
    	<url-pattern>/*</url-pattern>
    </filter-mapping>
```
以上通过 SpringJFinalFilter 过滤器加载 spring 。
其中 JfinalConfig 自动注册 springbean（JfinalConfig 中可以使用 注解注入属性哦，或者 实现 ApplicationContextAware  接口自动注入 ApplicationContext）。

## 2、HelloController
``` java
    @org.springframework.stereotype.Controller
    public class HelloController extends Controller {
    	public void index() {
    		renderText("Hello JFinal World.");
    	}
    }
```
以上也开源通过 xml 配置 controller.

## 3、页面使用 ApplicationContext
添加 ContextSpringHandler 就可以了。使用方式可以参照 ContextPathHandler。

## 4、spring 事物管理
```xml
    <bean id="druidDataSource" class="com.alibaba.druid.pool.DruidDataSource">
		<property name="username" value="${db.userName}" />
		<property name="password" value="${db.passWord}" />
		<property name="url" value="${db.jdbcUrl}" />
		<property name="driverClassName" value="${db.driverClassName}" />
		<property name="initialSize" value="${db.initialSize}" />
		<property name="maxActive" value="${db.maxActive}" />
		<property name="minIdle" value="${db.minIdle}" />
	</bean>
	<!-- spring 事物管理 ,ActiveRecordPlugin可以获得此 dataSource 可以把事务交给spring 管理 -->
	<bean id="dataSourceProxy" class="org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy">
		<property name="targetDataSource" ref="druidDataSource" />
	</bean>

	<!-- ================================事务相关控制================================================= -->
	<bean name="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
		<property name="dataSource" ref="dataSourceProxy"></property>
	</bean>

	<tx:advice id="txAdvice" transaction-manager="transactionManager">
		<tx:attributes>
			<tx:method name="delete*" propagation="REQUIRED" read-only="false" />
			<tx:method name="insert*" propagation="REQUIRED" read-only="false" />
			<tx:method name="update*" propagation="REQUIRED" read-only="false" />

			<tx:method name="find*" propagation="SUPPORTS" />
			<tx:method name="get*" propagation="SUPPORTS" />
			<tx:method name="select*" propagation="SUPPORTS" />
		</tx:attributes>
	</tx:advice>

	<!-- 把事务控制在Service层 -->
	<aop:config>
		<aop:pointcut id="pc" expression="execution(public * demo.service.*.*(..))" />
		<aop:advisor pointcut-ref="pc" advice-ref="txAdvice" />
	</aop:config>
```
以上是 xml 配置方式。
重点是在 ActiveRecordPlugin 中注入 代理数据源。

## 5、更多支持
- JFinal 官方网站  [http://www.jfinal.com](http://www.jfinal.com/) 
- 关注官方微信号马上体验 demo 功能  
![JFinal Weixin SDK](http://static.oschina.net/uploads/space/2015/0211/181947_2431_201137.jpg)

- spring-jfinal 下载地址[http://pan.baidu.com/s/1i3Dw08P#path=%252Fspring-jfinal]
- oschina 开源社区 maven库[http://maven.oschina.net/index.html#nexus-search;quick~spring-jfinal]
- oschina 开源社区 [http://www.oschina.net/p/spring-jfinal?from=mail-notify]
