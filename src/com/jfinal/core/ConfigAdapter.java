package com.jfinal.core;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;

/**
 * Config 适配器(主要原因是Config作用于太小了)
 * 使用 adapter 必须在 jfinal.init 之后
 * @ClassName: ConfigAdapter 
 * @author huangx
 * @date 2015年2月6日 上午9:05:26
 */
public class ConfigAdapter { 

	// my
	private Boolean devMode;
	// Field
	private Constants constants;
	private Routes routes;
	private Plugins plugins;
	private Interceptors interceptors;
	private Handlers handlers;

	private ConfigAdapter() {
	}

	public static ConfigAdapter on() {
		return new ConfigAdapter();
	}

	public Constants getConstants() {
		if (constants != null) return constants;
		return constants = Config.getConstants();
	}

	public Routes getRoutes() {
		if (routes != null) return routes;
		return routes = Config.getRoutes();
	}

	public Plugins getPlugins() {
		if (plugins != null) return plugins;
		return plugins = Config.getPlugins();
	}

	public Interceptors getInterceptors() {
		if (interceptors != null) return interceptors;
		return interceptors = Config.getInterceptors();
	}

	public Handlers getHandlers() {
		if (handlers != null) return handlers;
		return handlers = Config.getHandlers();
	}

	/*------------------------- my ----------------------*/
	public Boolean getDevMode() {
		if (devMode != null) return devMode;
		return devMode = getConstants().getDevMode();
	}
}
