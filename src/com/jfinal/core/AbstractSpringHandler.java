package com.jfinal.core;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import com.jfinal.handler.Handler;
import com.jfinal.util.SpringUtils;
import com.jfinal.util.SpringUtils.Scope;

/**
 * spring 必要的 handler
 * @ClassName: AbstractSpringHandler 
 * @author huangx
 * @date 2015年2月4日 下午12:01:57
 */
public abstract class AbstractSpringHandler extends Handler {

	// spring
	private ApplicationContext ctx;

	public AbstractSpringHandler(ApplicationContext ctx) {
		Assert.notNull(ctx);
		this.ctx = ctx;
	}

	/**
	 * @Title: 获得 Controller
	 * @param controllerClass
	 * @return
	 */
	public Controller getController(Class<? extends Controller> controllerClass) {
		Assert.notNull(controllerClass);
		Controller ctrl = null;
		try {
			// controller 
			ctrl = ctx.getBean(controllerClass);
		} catch (Exception e) {
			// 动态注册
			if (ctrl == null) {
				BeanDefinitionBuilder builder = SpringUtils.genericBean(controllerClass);
				AbstractBeanDefinition beanDefinition = builder.getRawBeanDefinition();
				// 每次请求都创建一个实例 符合 jfinal 创建 Controller方式 (jfinal 是每次请求都创建实例).
				beanDefinition.setScope(Scope.prototype);
				SpringUtils.registerBean(ctx, controllerClass.getName(), beanDefinition);
				ctrl = ctx.getBean(controllerClass);
			}
		}

		Assert.notNull(ctrl);
		return ctrl;
	}
}
