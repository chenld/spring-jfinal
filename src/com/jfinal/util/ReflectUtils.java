package com.jfinal.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Reflect
 * @ClassName: ReflectUtils
 * @author huangx
 * @date 2015年1月30日 下午2:58:01
 */
@SuppressWarnings("unchecked")
public class ReflectUtils {

	/*-------------------------------- Field -----------------------------------*/
	/**
	 * @Title: 赋值 field
	 * @param f Field
	 * @param obj 对象
	 * @param value 值
	 */
	public static void set(Object obj, Field f, Object value) {
		try {
			f.set(obj, value);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: 获得值
	 * @param f Field
	 * @param obj 对象
	 * @return 值
	 */
	public static <T> T get(Object obj, Field f) {
		try {
			return (T) f.get(obj);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: 获得 Field
	 * @param cls Class
	 * @param name 名称
	 * @return Field
	 */
	public static Field field(Class<?> cls, String name) {
		try {
			return cls.getField(name);
		} catch (Exception e) {
			try {
				Field f = cls.getDeclaredField(name);
				f.setAccessible(true);
				return f;
			} catch (Exception e1) {
				throw new IllegalArgumentException(e1);
			}
		}
	}

	/*-------------------------------- Instance -----------------------------------*/
	/**
	 * @Title: 新建 实例
	 * @param cls
	 * @return
	 */
	public static <T> T newInstance(Class<T> cls) {
		try {
			return cls.newInstance();
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/*-------------------------------- Method -----------------------------------*/
	/**
	 * @Title: 获得   Method
	 * @param cls
	 * @param name
	 * @param parameterTypes
	 * @return Method 
	 * @throws
	 */
	public static Method method(Class<?> cls, String name, Class<?>... parameterTypes) {
		try {
			return cls.getMethod(name);
		} catch (Exception e) {
			try {
				Method m = cls.getDeclaredMethod(name);
				m.setAccessible(true);
				return m;
			} catch (Exception e1) {
				throw new IllegalArgumentException(e1);
			}
		}
	}

	/**
	 * @Title: 执行 Method  
	 * @param m
	 * @param obj
	 * @param args
	 * @return T 
	 * @throws
	 */
	public static <T> T invoke(Object obj, Method m, Object... args) {
		try {
			return (T) m.invoke(obj, args);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/*-------------------------------- Class -----------------------------------*/
	/**
	 * @Title: toClass
	 * @param className
	 * @return
	 */
	public static Class<?> forName(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @Title: toClasses
	 * @param classNames
	 * @return
	 */
	public static Class<?>[] forNames(String... classNames) {
		Class<?>[] classes = new Class[classNames.length];
		for (int i = 0; i < classNames.length; i++) {
			classes[i] = forName(classNames[i]);
		}
		return classes;
	}
}
